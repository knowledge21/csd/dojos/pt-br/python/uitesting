# teste_site_sidia.py
from unittest import TestCase
from selenium import webdriver


class TesteSiteSidia(TestCase):
    def testa_formulario_de_contato_sem_preenchimento_de_campos_obrigatorios(self):
        chrome = webdriver.Chrome()

        chrome.implicitly_wait(10)

        chrome.get('https://www.sidia.com/')

        menu_contato = chrome.find_element_by_css_selector('#menu-item-23 > a')
        menu_contato.click()

        campo_assunto = chrome.find_element_by_css_selector('#wpcf7-f5-p18-o1 > form > p:nth-child(4) > label > span > input')
        campo_assunto.send_keys('Teste')

        campo_mensagem = chrome.find_element_by_css_selector('#wpcf7-f5-p18-o1 > form > p:nth-child(5) > label > span > textarea')
        campo_mensagem.send_keys('Teste sem preencher')

        botao_enviar = chrome.find_element_by_css_selector(
            '#wpcf7-f5-p18-o1 > form > p:nth-child(6) > input')
        botao_enviar.click()

        span_mensagem = chrome.find_element_by_css_selector(
            '#wpcf7-f5-p18-o1 > form > div.fusion-alert.alert.custom.alert-custom.fusion-alert-center.wpcf7-response-output.wpcf7-display-none.fusion-alert-capitalize.alert-dismissable.wpcf7-validation-errors.error.fusion-danger > div > span.fusion-alert-content')
        mensagem_esperada = 'Um ou mais campos possuem um erro. Verifique e tente novamente.'

        assert span_mensagem.text.upper() == mensagem_esperada.upper()