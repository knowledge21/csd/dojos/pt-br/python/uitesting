from unittest import TestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import selenium.webdriver.support.expected_conditions as EC

class TesteAssinarGloboPlay(TestCase):
    def testa_email_invalido_ao_assinar(self):
        chrome = webdriver.Chrome()

        chrome.get('https://globoplay.globo.com/')

        botao_prosseguir = chrome.find_element_by_css_selector(
            '#cookie-banner-lgpd > div > div.cookie-banner-lgpd_button-box > button')
        botao_prosseguir.click()

        img_avatar = chrome.find_element_by_css_selector(
            '#app > div > div > div > header > div.toolbar__container > div.toolbar__user-area > div.toolbar__avatar-container')

        img_avatar.click()

        botao_assine = chrome.find_element_by_css_selector(
            '#app > div > div > div > header > div.toolbar__container > div.toolbar__user-area > div.toolbar__user-area__container.header__fade-menu-dropdown > div > div.menu-header > a > div')
        botao_assine.click()

        WebDriverWait(chrome, 60).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '#app > div > div > div > div > span > div > div > div.subscribe-view__content > div.plans-bar > div:nth-child(2) > div.action-link')))

        botao_7dias = chrome.find_element_by_css_selector(
            '#app > div > div > div > div > span > div > div > div.subscribe-view__content > div.plans-bar > div:nth-child(2) > div.action-link')
        botao_7dias.click()

        WebDriverWait(chrome, 60).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '#login')))

        campo_login = chrome.find_element_by_css_selector('#login')
        campo_login.send_keys('teste@teste.com')

        campo_senha = chrome.find_element_by_css_selector('#password')
        campo_senha.send_keys('1234')

        botao_entrar = chrome.find_element_by_css_selector('#login-form > div.actions > button')
        botao_entrar.click()

        WebDriverWait(chrome, 60).until(EC.element_to_be_clickable((By.CSS_SELECTOR,
                                                                    '#login-form > div:nth-child(3) > div.validation-message.login-field > span')))

        msg_erro = chrome.find_element_by_css_selector(
            '#login-form > div:nth-child(3) > div.validation-message.login-field > span')

        assert msg_erro.text == 'Seu usuário ou senha estão incorretos.'